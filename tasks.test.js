const { createPromise, createResolvedPromise, createResolvedPromiseWithData,
    createRejectedPromise, createRejectedPromiseWithError, fulfilledOrNotFulfilled, timer } = require('./tasks');

describe('createPromise', () => {
    test('возвращает промис', () => {
        expect(typeof createPromise()).toBe('object');
        expect(createPromise() instanceof Promise).toBe(true);
    });
});

describe('createResolvedPromise', () => {
    test('возвращает промис', () => {
        expect(typeof createResolvedPromise()).toBe('object');
        expect(createResolvedPromise() instanceof Promise).toBe(true);
    });
    test('возвращает выполненный промис', () => {
        return expect(createResolvedPromise()).resolves.toBe(undefined);
    });
});

describe('createResolvedPromiseWithData', () => {
    test('возвращает промис', () => {
        expect(typeof createResolvedPromiseWithData()).toBe('object');
        expect(createResolvedPromiseWithData() instanceof Promise).toBe(true);
    });
    test('возвращает выполненный промис с правильным текстом', () => {
        return expect(createResolvedPromiseWithData()).resolves.toBe('Готово!');
    });
});

describe('createRejectedPromise', () => {
    test('возвращает промис', () => {
        expect(typeof createRejectedPromise()).toBe('object');
        expect(createRejectedPromise() instanceof Promise).toBe(true);
    });
    test('возвращает отклоненный промис', () => {
        return expect(createRejectedPromise()).rejects.toBe(undefined);
    });
});

describe('createRejectedPromiseWithError', () => {
    test('возвращает промис', () => {
        expect(typeof createRejectedPromiseWithError()).toBe('object');
        expect(createRejectedPromiseWithError() instanceof Promise).toBe(true);
    });
    test('возвращает отклоненный промис с ошибкой', () => {
        return expect(createRejectedPromiseWithError()).rejects.toMatchObject(new Error('Что-то пошло не так'));
    });
});

describe('fulfilledOrNotFulfilled', () => {
    test('возвращает промис', () => {
        expect(typeof fulfilledOrNotFulfilled(true)).toBe('object');
        expect(fulfilledOrNotFulfilled(true) instanceof Promise).toBe(true);
    });
    test('возвращает выполненный промис, если передать true', () => {
        return expect(fulfilledOrNotFulfilled(true)).resolves.toBe(undefined)
    });
    test('возвращает отклоненный промис, если передать false', () => {
        return expect(fulfilledOrNotFulfilled(false)).rejects.toBe(undefined)
    });
});

describe('timer', () => {
    test('возвращает промис', () => {
        expect(typeof timer()).toBe('object');
        expect(timer() instanceof Promise).toBe(true);
    });
    test('возвращает выполненный промис', () => {
        jest.useFakeTimers();
        const timerPromise = timer();
        jest.runAllTimers();
        return expect(timerPromise).resolves.toBe(undefined)
    });
    test('реализует задержку в 3 секунды', () => {
        jest.useFakeTimers();
        timer();
        expect(setTimeout).toHaveBeenCalledTimes(1);
        expect(setTimeout).toHaveBeenLastCalledWith(expect.any(Function), 3000);
    });
});

